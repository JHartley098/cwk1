package comp2931.cwk1;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class DateTest {
    @Test
    public void dateToString() {
        Date dateOne = new Date(2017, 10, 12);
        Date dateTwo = new Date(1997, 3, 23);
        Date dateThree = new Date(1634, 11, 2);

        assertThat(dateOne.toString(), is("2017-10-12"));
        assertThat(dateTwo.toString(), is("1997-03-23"));
        assertThat(dateThree.toString(), is("1634-11-02"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void dayTooHigh() {
        new Date(2014, 2, 32);
        new Date(1976, 5, 54);
    }

    @Test(expected = IllegalArgumentException.class)
    public void monthTooHigh() {
        new Date(2017, 13, 12);
        new Date(1943, 34, 24);
    }

    @Test(expected = IllegalArgumentException.class)
    public void yearTooLarge() {
        new Date(21973, 13, 12);
        new Date(11111, 13, 12);
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeDay() {
        new Date(1738, 2, 0);
        new Date(1297, 5, -12);
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeMonth() {
        new Date(3283, 0, 15);
        new Date(1332, -1, 21);
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeYear() {
        new Date(0, 12, 24);
        new Date(-1846, 5, 3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void monthsWith28Days() {
        new Date(1345, 2, 29);
    }

    @Test(expected = IllegalArgumentException.class)
    public void monthsWith30Days() {
        new Date(2354, 4, 31);
        new Date(1012, 6, 31);
        new Date(2453, 9, 31);
        new Date(1464, 11, 31);
    }

    @Test(expected = IllegalArgumentException.class)
    public void monthsWith31Days() {
        new Date(1453, 1, 32);
        new Date(3453, 3, 32);
        new Date(5423, 5, 32);
        new Date(2245, 7, 32);
        new Date(1324, 8, 32);
        new Date(1668, 10, 32);
        new Date(1995, 12, 32);
    }

    @Test
    public void equals() {
        Date dateOne = new Date(1997, 5, 15);

        assertTrue(dateOne.equals(dateOne));
        assertTrue(dateOne.equals(new Date(1997, 5, 15)));
        assertFalse(dateOne.equals(new Date(2001, 9, 4)));
        assertFalse(dateOne.equals(new Date(1997, 5, 1)));
    }

    @Test
    public void dayOfYear() {
        Date dateOne = new Date(1923, 1, 1);
        Date dateTwo = new Date(2006, 5, 14);

        assertThat(dateOne.getDayOfYear(), is(1));
        assertThat(dateTwo.getDayOfYear(), is(134));
    }

    @Test
    public void leapYear() {
        new Date(2000, 2, 29);
        new Date(2012, 2, 29);
        new Date(2016, 2, 29);
    }
}