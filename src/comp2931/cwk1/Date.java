// Class for COMP2931 Coursework 1

package comp2931.cwk1;

import java.util.Calendar;

/**
 * Simple representation of a date.
 */
public class Date {

  private int year;
  private int month;
  private int day;

  int[] daysInMonth = {(31),(28),(31),(30),(31),(30),(31),(31),(30),(31),(30),(31)};

  /**
   * Creates a date using the given values for year, month and day.
   *
   * @param y Year
   * @param m Month
   * @param d Day
   */
  public Date(int y, int m, int d) {
    setYear(y);
    setMonth(m);
    setDay(d);
  }

  /**
   * Creates a date with the values of the current date values.
   */
  public Date() {
    Calendar today = Calendar.getInstance();

    year = today.get(Calendar.YEAR);
    month = today.get(Calendar.MONTH) + 1;
    day = today.get(Calendar.DAY_OF_MONTH);
  }

  /**
   * Set field year if valid
   *
   * @param year Year
   */
  public void setYear(int year) {
    if (year > 0 && year <= 9999) {
      this.year = year;
    }
    else { throw new IllegalArgumentException("Invalid Year"); }
  }

  /**
   * Set field month if valid
   *
   * @param month Month
   */
  public void setMonth(int month) {
    if (month > 0 && month <= 12) {
      this.month = month;
    }
    else {
      throw new IllegalArgumentException("Invalid Month");
    }
  }

  /**
   * Set field day if valid
   *
   * @param day Day
   */
  public void setDay(int day) {
    if (getYear() % 4 == 0 && getMonth() == 2) {
      if (day > 0 && day <= 29) {
        this.day = day;
      }
    }
    else if (day > 0 && day <= (daysInMonth[getMonth() - 1])) {
        this.day = day;
    }
    else {
      throw new IllegalArgumentException("Invalid Date");
    }
  }

  /**
   * Returns the year component of this date.
   *
   * @return Year
   */
  public int getYear() {
    return year;
  }

  /**
   * Returns the month component of this date.
   *
   * @return Month
   */
  public int getMonth() {
    return month;
  }

  /**
   * Returns the day component of this date.
   *
   * @return Day
   */
  public int getDay() {
    return day;
  }

  /**
   * Provides a string representation of this date.
   *
   * ISO 8601 format is used (YYYY-MM-DD).
   *
   * @return Date as a string
   */
  @Override
  public String toString() {
    return String.format("%04d-%02d-%02d", year, month, day);
  }

  /**
   * Compares two date objects for equality of state.
   *
   * @param other The second Date object.
   * @return True or False.
   */
  @Override
  public boolean equals(Object other) {
    if (this == other) return true;
    if (other == null || getClass() != other.getClass()) return false;

    Date date = (Date) other;

    if (year != date.year) return false;
    if (month != date.month) return false;
    return day == date.day;
  }

  /**
   * Overriding default hashcode
   *
   * @return Hashcode
   */
  @Override
  public int hashCode() {
    int result = year;
    result = 31 * result + month;
    result = 31 * result + day;
    return result;
  }

  /**
   * Works out the day of the year as an integer.
   *
   * @return Day of the year
   */
  public int getDayOfYear() {
    int month = getMonth(), i, dayOfYear;
    i = dayOfYear = 0;

    while (i < month - 1) {
      dayOfYear += daysInMonth[i];
      i++;
    }
    return dayOfYear + day;
  }
}